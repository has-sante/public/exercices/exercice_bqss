# Exercice BQSS 📊

La [BQSS](https://www.data.gouv.fr/fr/datasets/base-sur-la-qualite-et-la-securite-des-soins-anciennement-scope-sante/) regroupe les informations sur la qualité des hôpitaux et des cliniques produites par la Haute Autorité de Santé.

Une documentation détaillée est également disponible [en ligne](https://has-sante.pages.has-sante.fr/public/bqss/).

# Exemple de problématique métier

Une personne travaillant pour une institution publique de la Santé, souhaite utiliser les données de qualité et de sécurité des soins, produites par la HAS, dans le cadre d'une étude interne.
Elle souhaite, par exemple, répondre aux questions suivantes:

- Pour une année donnée, comparer deux établissements d'une catégorie donnée, sur un indicateur précis.
- Évaluer la progression, au fil du temps, d'un établissement sur la base d'un IQSS Esatis.
- Mesurer la distribution des résultats de certification (référentiel 2014) par région.
- Y a-t-il un lien entre le volume d'une activité de soin (SAE) et le résultat d'un IQSS ?

# But de l'exercice 🚀

Utilisez les données de la BQSS pour représenter l'évolution, au cours du temps, du positionnement d'un établissement de la catégorie FINESS `1101` ("Centres Hospitaliers Régionaux") pour l'IQSS Esatis `taux_reco_brut_48h` (Pourcentage de patients, recommandant certainement l’établissement, pour le secteur "hospitalisation de plus de 48h").

Les éléments qui nous intéressent dans cet exercice sont:

- La capacité à appréhender un nouveau jeu de données
- La capacité à identifier les spécificités techniques d'un jeu de données
- La capacité à réaliser des choix pragmatiques dans les traitements effectués

# En pratique 🛠

Un rendu sous la forme d'un notebook Jupyter et utilisant Python sera à privilégier.
À défaut, vous pouvez utiliser les technologies de votre choix pour traiter l'exercice.

Ce travail devrait prendre entre 2h et 3h de temps.

Le partage de votre travail se fera sur un dépôt personnel qui clonera ce dépôt.
Vous nous communiquerez le lien de ce dépôt au moins 24h avant l'entretien.

Une partie de l'entretien sera consacrée à une discussion autour de votre réponse à cet exercice.
